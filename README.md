# Ansible Dynamic Inventory script for vRealize Automation

A [python](https://www.python.org/) script to extract data from [**vRealize Automation**](https://docs.vmware.com/en/vRealize-Automation/index.html) **(vRA)** and generate the inventory data for [**Ansible**](https://www.ansible.com/)

# Instalation
The following script and it's prerequisites can be installed using the prebuild *install.sh* script!
The default script install path will be: ***/usr/sbin/vra_dynamic_invnetory***

**Script Usage:**
```
Command: install.sh (options)
Options:
  -h|--help                Provides the help menu
  -v|--version             Provides the version information
  -c|--custom_environmnet  Specify custom environmnet (more information regarding this option
                           will be available in a future version)
Usage example: ./install -c platon
```

**NOTE:** This is *install.sh* script will only work for centOS/Rhel OS !!! Also for custom environment please use the *-c* option!!!

# Requirements
Python packages : ***requests***,  ***chardet***,  ***urllib3***


# Usage 
The following script uses the following **OS Environment** variables in order to connect to the **vRA** server.

| Variable NAME | Usage EXAMPLE | Description |
|--|--|--|
| VRA_FQDN  | *vra.omdevinfra.local* | the **FQDN/IP** of the **vRA** server (with no / or *http/https* header) |
| VRA_USERNAME | *wpnv2112@omdevinfra.local* | the **Username** used to login on the **vRA** server (with the tralling domain) |
| VRA_PASSWORD | *Password123!* | the Password in clear text used to login on the **vRA** server|
| VRA_TENNANT | *vsphere.local* | the **Tennant** used on the **vRA** ( for single tennant deployments use: *vsphere.local* |
| VRA_ACTIVE_DEPLOYMENT | *wpnv2112_testcopy-03344126* | the name of the deployment from which you want to extract the data |

These variable can be put in a credential file and then sourced to the **OS Environment**.  An example can be found bellow: 

```/bin/bash
export VRA_FQDN='vra.omdevinfra.local'
export VRA_USERNAME='wpnv2112@omdevinfra.local'
export VRA_PASSWORD='Password123!'
export VRA_TENNANT='vsphere.local'

export VRA_ACTIVE_DEPLOYMENT='wpnv2112_testcopy-03344126'
```
# Constraints
The script has the following constraints:

 - The **Ansible** group will be the **Machine Component/Cluster/Group** name mentioned in the Blueprint. You will not be able to add a VM into multiple Ansible groups
 - The LB (On-Demand-LoadBalancer) cannot have hyphens ('-') in the Blueprint Component Name. If not respected then the Ansible **LB VIP** name will be incorrect 
> LB VIP format : *vip_<<LB_component_name>>*; **Ex:** *vip_testLB*
- The **IP** extracted from the **vRA** (used by ansible to connect to the VM) will the the first IP from the first interface (nic0/eth0) 


# Output
The script will generate the following inventory structure: 
```json
[root@localhost vRA_dynamic_inventory]# source credentials
[root@localhost vRA_dynamic_inventory]# ./vra_dynamic_inventory.py
{
    "Ker_ND": {
        "hosts": [
            "BGOrangeMon0384",
            "BGOrangeMon0385"
        ]
    },
    "all": {
        "hosts": [
            "BGOrangeMon0390",
            "BGOrangeMon0391",
            "BGOrangeMon0389",
            "BGOrangeMon0384",
            "BGOrangeMon0393",
            "BGOrangeMon0386",
            "BGOrangeMon0382",
            "BGOrangeMon0383",
            "BGOrangeMon0387",
            "BGOrangeMon0381",
            "BGOrangeMon0392",
            "BGOrangeMon0385",
            "BGOrangeMon0388"
        ],
        "vars": {
            "vra_deployment_name": "Kermit_vRA_POC-43856748",
            "vra_deployment_id": "9699dc31-b337-4efc-b6bb-91a799786598",
            "vip_Ker_MA_LB": "10.192.107.128",
            "vip_Ker_IP_LB": "10.192.107.127",
            "vip_Ker_GG_LB": "10.192.107.129"
        }
    },
    "Ker_IP": {
        "hosts": [
            "BGOrangeMon0386",
            "BGOrangeMon0387"
        ]
    },
    "_meta": {
        "hostvars": {
            "BGOrangeMon0389": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "3070431e-5d97-4b45-ad5e-c2045535ffcd",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.23"
            },
            "BGOrangeMon0388": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "fb885477-9cd7-4ea7-b6cc-df7338f01961",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.25"
            },
            "BGOrangeMon0390": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "0f473ef6-3c14-4ae4-b78b-8a9510e6dd19",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.27"
            },
            "BGOrangeMon0391": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "160794b6-8582-45b1-af67-c85ef551948b",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.26"
            },
            "BGOrangeMon0381": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "7d410e5d-c275-4851-ad2d-5b3f15c4b8fd",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.192.107.132"
            },
            "BGOrangeMon0393": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "4613c6c3-3de6-41b0-b628-2451ff7c14c9",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.29"
            },
            "BGOrangeMon0387": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "635353e8-7708-4dd0-89b0-fdb08e079515",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.21"
            },
            "BGOrangeMon0386": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "487d308f-5e29-408c-ad46-e8ab436aaf1f",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.22"
            },
            "BGOrangeMon0385": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "befa5733-81d7-461b-b99f-dbaeead45c0c",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.20"
            },
            "BGOrangeMon0384": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "3d54ee2e-77dc-4edb-b67f-4e339fec1ff2",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.19"
            },
            "BGOrangeMon0383": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "4fb543cb-94c3-4dbb-9c17-3c5ad1b4d28a",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.192.107.131"
            },
            "BGOrangeMon0382": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "49fd78da-de00-4855-ad35-7cbf4dde2dc3",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.192.107.130"
            },
            "BGOrangeMon0392": {
                "vra_machine_parrent_id": "9699dc31-b337-4efc-b6bb-91a799786598",
                "vra_machine_request_id": "e1b37950-2ee3-431a-a858-7dc9e45fbd5c",
                "vra_machine_id": "87b5f419-c09e-4fd0-8760-52ed571eae88",
                "vra_machine_parrent_name": "Kermit_vRA_POC-43856748",
                "ansible_host": "10.0.5.28"
            }
        }
    },
    "Ker_IG": {
        "hosts": [
            "BGOrangeMon0393",
            "BGOrangeMon0392"
        ]
    },
    "Ker_NP": {
        "hosts": [
            "BGOrangeMon0389",
            "BGOrangeMon0388"
        ]
    },
    "Ker_GG": {
        "hosts": [
            "BGOrangeMon0390",
            "BGOrangeMon0391"
        ]
    },
    "Ker_Master": {
        "hosts": [
            "BGOrangeMon0382",
            "BGOrangeMon0383",
            "BGOrangeMon0381"
        ]
    }
}

```
