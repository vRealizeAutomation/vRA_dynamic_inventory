#!/bin/bash

py_cur_ver=$(python -V 2>&1 | grep -Po '(?<=Python )(.+)')
py_req_ver="2.7.5"
INSTALL_path="/usr/sbin"
INSTALL_app="vra_dynamic_inventory"


print_version() {
  echo Version $(cat $1 | grep -ie "^#ver*"| cut -f2 -d " ");
}

print_help() {
  echo "
Command: install.sh (options)
Options: 
  -h|--help                Provides the help menu
  -v|--version             Provides the version information
  -c|--custom_environmnet  Specify custom environmnet (more information regarding this option
                           will be available in a future version)
Usage example: ./install -c platon
"
}

#Check if python
check_prog() {
  local PKG
  if ! which $1 &> /dev/null; then
    if [ "$2" == "0" ];then echo "ERROR: Program: $1 - could not be installed! Please manually solve the issiu !"; exit 1; fi;
    if [ ! -z "$2" ]; then PKG=$2; else PKG=$1; fi;
    echo WARN:Program: $PKG - is not installed\! Attempting install ...\!;
    install_prog $PKG;
    check_prog $1 0;
  else
    echo "INFO:Program: $1 - installed!";
  fi
}

check_os() {
  local UNAME_OS=$(uname -s);UNAME_V=$(uname -r)
  local __resultvar=$1
  local myresult
  case $UNAME_OS in
      Linux)
      case $UNAME_V in
         *.el7.*)
         myresult="rhel7"
         if [[ "$__resultvar" ]]; then
           eval $__resultvar="'$myresult'"
         else
           echo "$myresult"
         fi
         ;;
      esac 
      ;;
      *)
      echo "ERROR! Operating system unrecognize: $(uname -s)";
      exit 1;
      ;;
  esac
}
#Install program
install_prog() {
  local OS=$(check_os)
  case $OS in 
     rhel*)
     if [ -z "$CUSTOM_ENV" ] && which subscription-manager &> /dev/null; then 
        echo "WARN:Detected subscription manager! Please provide valide credentials if prompted!"
        subscribe_rhel; 
        #Fix - beta rpms
        local BETA=$(subscription-manager repos --list-enabled | grep -ie "Repo ID:.*-beta-.*" | cut -f2 -d ':' | awk '{print $1}')
        if [ ! -z "$BETA" ]; then 
           echo "INFO:Found beta repo active!Disabling it ....";
           subscription-manager repos --disable=$BETA;
        fi
     fi
     if [ "$2" == "upgrade" ]; then yum update -y -q -e 0 $1; fi;
     yum install -y -q -e 0 $1 
     ;;
  esac
}

subscribe_rhel() {
  local STATUS=$(subscription-manager status | grep -i "Overall Status" | cut -f 2 -d ':')
  case $STATUS in
    *Unknown)
    subscription-manager register
    subscribe_rhel
    ;;
    *Invalid)
    subscription-manager attach
    ;;
    *Current)
    echo "INFO:Found valid license...Continuing install/upgrade... "
    ;;
  esac 
}

#Check version function
check_ver() {
 local currentver=$1
 local requiredver=$2
 local progname=$3
 if [ "$(printf "$requiredver\n$currentver" | sort -V | head -n1)" == "$currentver" ] && [ "$currentver" != "$requiredver" ]; then 
        echo "WARN:Required $progname version: $requiredver! Found version: $currentver! Trying to upgrade...";
        if [ "$4" == "0" ]; then 
            echo "ERROR: Application $progname:$currentver could not be upgraded to => $requiredver! Please manually invetigate the problem !";
            exit 1;
        fi
        install_prog $progname "upgrade"
        check_ver $currentver $requiredver $progname 0
 else
        echo "INFO:Required version met ! $progname:$currentver >= $requiredver";
 fi
}

check_pip_pkg() {
  if [ "$2" == "0" ]; then echo "ERROR:Python pkg; $1 - could not be installed! Please install it manually!"; exit 1; fi;
  pip install --upgrade pip &> /dev/null;
  if ! pip list --format=legacy | grep -ie "^$1 *" &> /dev/null; then
     echo "WARN:Python pkg: $1 - is not installed! Attempting to install it...";
     pip install $1 &> /dev/null;
     check_pip_pkg $1 0;
  else 
     echo "INFO:PYTHON pkg: $1 - installed!";
     pip install -U $1 &> /dev/null; 
  fi
}

install_epel() {
if [ "$1" != "disable" ]; then
 if ! yum repolist | grep epel &> /dev/null; then
   if ! ls /etc/yum.repos.d/ | grep -ie "^epel.repo$" &> /dev/null; then
    if [ "$1" == 0 ]; then 
       echo "ERROR: EPEL repo could not be installed! Please install it manually!";
       exit 1;
    fi
    echo "WARN:EPEL repo is not installed! Trying to install it ..";
    rpm -ivh "https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(echo $OS | tr -dc '0-9').noarch.rpm" &> /dev/null;
    install_epel;
   else
    echo "INFO:EPEL repo exists but not enabled! Temporary enabling it ..";
    yum-config-manager --enable epel &> /dev/null;
    enabled_repo=true;
    EPEL_STATUS="disabled";
   fi
 else
    echo 'INFO:EPEL repo - is installed!';
 fi
else
  yum-config-manager --disable epel &> /dev/null;
  echo "INFO:EPEL repo has been disabled!";
fi
}

check_rpm() {
if ! rpm -qa | grep -ie "^$1*" &> /dev/null ; then 
   if [ "$2" == "0" ]; then 
      echo "ERROR: RPM: $1 could not be installed! Please verify and install manually ... !";
      exit 1;
   fi
   yum install -y $1 &> /dev/null;
   check_rpm $1 0;
else
  echo "INFO: RPM $1 is already installed !";   
fi
}

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in 
   -v|--version)
   print_version "$DIR/$INSTALL_app.py";
   exit 0;
   ;;
   -h|--help)
   print_help;
   exit 0;
   ;;
   -c|--custom_environmnet)
   CUSTOM_ENV="$2"
   shift
   shift
   ;;
   *)
   echo "Unknown option: $1";
   echo "Please consult the help menu!";
   print_help;
   exit 1;
   ;;
esac
done 
set -- "${POSITIONAL[@]}" # restore positional parameters

check_prog "python"
check_ver $py_cur_ver $py_req_ver "python"

enabled_repo=false;
OS=$(check_os)
case $OS in
  rhel*)
  install_epel;
  check_rpm "python-chardet"
  check_rpm "python-urllib3"
  check_rpm "python-requests"
  ;;
esac

check_prog "pip" "python2-pip"
check_pip_pkg "chardet"
check_pip_pkg "urllib3"
check_pip_pkg "requests"

trigger='null';
trigger_ver='null';
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
rVER=$(cat $DIR/$INSTALL_app.py | grep -ie "^#ver *"|cut -f2 -d " ")
i=1;while [ ! -z $(echo $PATH | cut -f$i -d':') ];do 
  var=$(echo $PATH | cut -f$i -d':')
  if [ $var != "/root/bin" ] && ls $var | grep $INSTALL_app &> /dev/null; then 
     if cat $var/$INSTALL_app | grep -ie "^#ver*" &> /dev/null; then
         eVER=$(cat $var/$INSTALL_app | grep -ie "^#ver*"| cut -f2 -d " ")
         if [ "$(printf "$rVER\n$eVER" | sort -V | head -n1)" == "$eVER" ] && [ "$eVER" != "$rVER" ]; then
            cp $DIR/$INSTALL_app".py" $INSTALL_path/$INSTALL_app
            chmod ugo+x $INSTALL_path/$INSTALL_app
            trigger=1;trigger_ver=$eVER;
         else
            if [ "$trigger" == "null" ]; then trigger=0; fi
         fi
     fi
  fi
  let i=i+1
done

if [ "$EPEL_STATUS" == "disabled" ]; then install_epel "disable"; fi;
case $trigger in
   1)
   echo "INFO:Dynamic inventory script upgrade from version:$trigger_ver to:$rVER"
   ;;
   0)
   echo "INFO:Dynamic inventory script is already at the latest versions:$rVER"
   ;;
   null)
   echo "INFO:Dynamic inventory script installed succesfully!";
   cp $DIR/$INSTALL_app".py" $INSTALL_path/$INSTALL_app
   chmod ugo+x $INSTALL_path/$INSTALL_app
   ;;
esac


echo ""; echo "Dynamic inventory script instaleld in path:";
echo $INSTALL_path/$INSTALL_app
echo ""; echo "INSTALL SUCCEEDED!";
exit 0
