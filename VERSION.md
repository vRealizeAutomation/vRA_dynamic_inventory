v0.3 - Fixes + Enhancement
 - BUG/Enhancement #6 - add custom environment variable to remove subscription on rhel
 - Enhancement #5 - add help option and version + custom options 

v0.2 - Fixes + 1 Enhancement
 - BUG #2 - EPEL loop install
 - Enhance #1 - Create install script for prereq
 - BUG #3 - Install script error ... nothing installled if no previous version found
 - BUG #4 - Version check failed

v0.1 - Initial commit with docs

