#!/usr/bin/env python
################################################################################
# Dynamic inventory generation for Ansible
# Author george.oniceag@orange.com/george.oniceag@gmail.com
#
# This Python script generates a dynamic inventory based on vRealize Automation (vRA) deployments
#
# The script is passed via -i <script name> to ansible-playbook. Ansible
# recognizes the execute bit of the file and executes the script. It then
# queries nova via the novaclient module and credentials passed via environment
# variables -- see below.
# *** Requirements ***
# * Python: requests 
# * The environment variables: VRA_FQDN, VRA_USERNAME, VRA_USERNAME, VRA_USERNAME, VRA_ACTIVE_DEPLOYMENT
#
################################################################################
#vers 0.3 (alpha)
#

_api_vRA_resources = {
   'api': '/catalog-service/api/consumer/resources',
   'format': '?page=1&limit=50000000'
 }
_api_vRA_network='/network-service/api/load-balancers'

import sys, os, json
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

def getOsCredentialsFromEnvironment():
  credentials = {}
  try:
	credentials['USERNAME'] = os.environ['VRA_USERNAME']
	credentials['PASSWORD'] = os.environ['VRA_PASSWORD']
	credentials['TENNANT'] = os.environ['VRA_TENNANT']
        credentials['FQDN'] = os.environ['VRA_FQDN']
	credentials['DEPLOYMENT'] = os.environ['VRA_ACTIVE_DEPLOYMENT']
  except KeyError as e:
	print("ERROR: environment variable %s is not defined" % e)
	sys.exit(-1)
  return credentials

def vra_auth(vrafqdn,user,password,tenant):
  url = "https://{}/identity/api/tokens".format(vrafqdn)
  payload = '{{"username":"{}","password":"{}","tenant":"{}"}}'.format(user, password, tenant)
  headers = {
        'accept': "application/json",
        'content-type': "application/json",
        }
  response = requests.request("POST", url, data=payload, headers=headers, verify=False)
  j = response.json()['id']
  auth = "Bearer "+j
  return auth 
  
def vra_get_api(token,apiPath,api,vrafqdn):
  url = "http://{}{}/{}".format(vrafqdn, apiPath, api)
  headers = {
        'accept': "application/json",
        'content-type': "application/json",
        'authorization': token
        }
  response = requests.request("GET",url, headers=headers, verify=False)
  return response

def vra_print(req):
  print(json.dumps(req, indent=4))

def getVRAresource(data, resource):
  return [ obj for obj in data if obj['resourceTypeRef'] is not None and obj['resourceTypeRef'].get('id') == resource ]
def getVMipvRA(vmdata):
  data = [ obj for obj in vmdata['resourceData']['entries'] if obj['key'] is not None and obj['key'] == "ip_address" ]
  return data[0]['value'].get('value')
def getVMclustervRA(vmdata):
  data = [ obj for obj in vmdata['resourceData']['entries'] if obj['key'] is not None and obj['key'] == "Component" ]
  return data[0]['value'].get('value')

def getVMHostVars(vmdata):
  data = {}
  data['ansible_host'] = [ obj for obj in vmdata['resourceData']['entries'] if obj['key'] is not None and obj['key'] == "ip_address" ][0]['value'].get('value')
  data['vra_machine_id'] = vmdata.get('id')
  data['vra_machine_parrent_id'] = vmdata['parentResourceRef'].get('id')
  data['vra_machine_parrent_name'] = vmdata['parentResourceRef'].get('label')
  data['vra_machine_request_id'] = vmdata.get('requestId')
  return data

def addVMtoInventory(group, name, inventory):
  host_group = inventory.get(group, {})
  hosts = host_group.get('hosts', [])
  hosts.append(name)
  host_group['hosts'] = hosts
  inventory[group] = host_group

def addLBtoInventory(group, name, vip, inventory):
  host_group = inventory.get(group, {})
  lb_vars = host_group.get('vars', {})
  lb_vars.update({name:vip})
  host_group['vars'] = lb_vars
  inventory[group] = host_group

def addVMHostVarsToInvetory(hostvars, name, inventory):
  inventory_host_vars = inventory['_meta']['hostvars'].get(name, {})
  inventory_host_vars.update(hostvars)
  inventory['_meta']['hostvars'][name] = inventory_host_vars

def add_allVMsToInventory(token,data_deployment,inventory,fqdn):
  global _api_vRA_resources
  _req_vra_deployment_vms = getVRAresource(data_deployment,"Infrastructure.Virtual")
  _vra_deployment_vms_list=[]
  for data in _req_vra_deployment_vms:
     _vm_data = vra_get_api(token,_api_vRA_resources['api'],data['id'],fqdn).json()
     if [ obj for obj in _vm_data['resourceData']['entries'] if obj['key'] is not None and obj['key'] == "MachineStatus" ][0]['value'].get('value') == "On":
       addVMtoInventory('all',data['name'],inventory)
       addVMtoInventory(getVMclustervRA(_vm_data),data['name'],inventory)
       addVMHostVarsToInvetory(getVMHostVars(_vm_data),data['name'],inventory)

def add_allLBsToInventory(token,data_deployment,inventory,fqdn):
  global _api_vRA_network
  _req_vra_deployment_lbs = getVRAresource(data_deployment,"Infrastructure.Network.LoadBalancer.NSX")
  _vra_deployemnt_lbs_list=[]
  for data in _req_vra_deployment_lbs:
      _lb = {}
      _lb['ip'] = [ obj for obj in data['resourceData']['entries'] if obj.get('key') == 'ip_address'][0]['value']['value']
      _lb['name'] = 'vip_' + [ obj for obj in data['resourceData']['entries'] if obj.get('key') == 'Name'][0]['value']['value'].split('-')[0]
      addLBtoInventory('all',_lb['name'],_lb['ip'],inventory)

def addvRaMetaAsGlovalVars(name,meta,inventory):
  host_group = inventory.get('all', {})
  meta_vars = host_group.get('vars', {})
  meta_vars.update({name:meta})
  host_group['vars'] = meta_vars
  inventory['all'] = host_group
      
def main(args):
  credentials = getOsCredentialsFromEnvironment()
  inventory = {}
  inventory['_meta'] = {}
  inventory['_meta']['hostvars'] = {}
  #Connect and get resource data
  _vra_connect_token = vra_auth(credentials['FQDN'],credentials['USERNAME'],credentials['PASSWORD'],credentials['TENNANT'])  
  _req_vra_resources = vra_get_api(_vra_connect_token,_api_vRA_resources['api'],_api_vRA_resources['format'],credentials['FQDN']).json()['content']
  _req_vra_deployment = [ obj for obj in _req_vra_resources if obj['parentResourceRef'] is not None and obj['parentResourceRef'].get('label') == credentials['DEPLOYMENT'] ]

  #Filter reosurces by reosurce type
  add_allVMsToInventory(_vra_connect_token,_req_vra_deployment, inventory, credentials['FQDN'])
  add_allLBsToInventory(_vra_connect_token,_req_vra_deployment, inventory, credentials['FQDN'])

  #Add meta info
  addvRaMetaAsGlovalVars('vra_deployment_name',credentials['DEPLOYMENT'], inventory)
  addvRaMetaAsGlovalVars('vra_deployment_id',_req_vra_deployment[0]['parentResourceRef'].get('id'), inventory) 
  vra_print(inventory)

if __name__ == "__main__":
    main(sys.argv)






